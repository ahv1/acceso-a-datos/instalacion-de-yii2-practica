<?php

namespace app\controllers;

use app\models\Provincias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProvinciasController implements the CRUD actions for Provincias model.
 */
class ProvinciasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Provincias models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Provincias::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'provincia' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Provincias model.
     * @param string $provincia Provincia
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($provincia)
    {
        return $this->render('view', [
            'model' => $this->findModel($provincia),
        ]);
    }

    /**
     * Creates a new Provincias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Provincias();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'provincia' => $model->provincia]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Provincias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $provincia Provincia
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($provincia)
    {
        $model = $this->findModel($provincia);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'provincia' => $model->provincia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Provincias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $provincia Provincia
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($provincia)
    {
        $this->findModel($provincia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Provincias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $provincia Provincia
     * @return Provincias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($provincia)
    {
        if (($model = Provincias::findOne(['provincia' => $provincia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
