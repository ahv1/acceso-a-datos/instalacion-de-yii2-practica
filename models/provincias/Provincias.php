<?php

namespace app\models\provincias;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property string|null $autonomia
 * @property string $provincia
 * @property int|null $poblacion
 * @property int|null $superficie
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia'], 'required'],
            [['poblacion', 'superficie'], 'integer'],
            [['autonomia', 'provincia'], 'string', 'max' => 255],
            [['provincia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autonomia' => 'Autonomia',
            'provincia' => 'Provincia',
            'poblacion' => 'Poblacion',
            'superficie' => 'Superficie',
        ];
    }
}
